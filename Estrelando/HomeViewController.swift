//
//  HomeViewController.swift
//  Estrelando
//
//  Created by Web1 on 14/09/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class HomeViewController:BaseRootViewController, UIScrollViewDelegate{

    @IBOutlet weak var viewToCollection: UIView!
    @IBOutlet weak var viewToPageViewController: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
  
    var collectionView:HomeCollectionViewController!
    override func viewDidLoad() {
        super.viewDidLoad()

       let collection =  self.storyboard?.instantiateViewControllerWithIdentifier("homeCollection") as? HomeCollectionViewController
        self.viewToPageViewController.addSubview((collection?.view)!)
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
