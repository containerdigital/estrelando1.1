//
//  BaseRootViewController.swift
//  Estrelando
//
//  Created by Web1 on 14/09/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class BaseRootViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Set left bar button iten , on nav controller.
        let hamburguerItem = UIImage(named: "hmg")
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: hamburguerItem, style: UIBarButtonItemStyle.Plain, target: self, action: "toggleMenu")

        self.navigationItem.leftBarButtonItem?.tintColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.4)
        //Set Logo 'Estrelando' on nav controller.
        let logo = UIImage(named: "logo-estrelando")
        let imageView = UIImageView(image: logo)
        self.navigationController?.navigationItem.titleView = imageView
        self.navigationItem.titleView = imageView
        
        
        
    }
    
    
    
    
    func toggleMenu(){
        NSNotificationCenter.defaultCenter().postNotificationName("toggleMenu", object: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
