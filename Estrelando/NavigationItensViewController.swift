//
//  NavigationItensViewController.swift
//  Estrelando
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class NavigationItensViewController: UIViewController,UIPageViewControllerDelegate,UIPageViewControllerDataSource {
    
    var pageTitles:NSMutableArray!
    var images:NSMutableArray!
    var count = 0
    var pageViewController: UIPageViewController!
    var urlFoto : NSURL?
    var dataFoto:NSMutableData?
    var dictionaryImages:NSMutableArray!
    var dataDictionary:NSMutableDictionary?
    var index:Int = Int()
    var arrayDictionary:NSMutableArray?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.images = self.arrayDictionary
        self.pageTitles = self.arrayDictionary
        reset()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reset(){
        self.pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageViewController2") as! UIPageViewController
        let pageContentViewController = self.viewControllerAtIndex(self.index)
        self.pageViewController.setViewControllers([pageContentViewController!], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)
        self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        self.addChildViewController(pageViewController)
        self.view.addSubview(pageViewController.view)
        self.pageViewController.didMoveToParentViewController(self)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! ConteudoFotosNavegacaoViewController).pageIndex!
        index++
        if(index >= self.images.count){
            return nil
        }
        return self.viewControllerAtIndex(index)
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! ConteudoFotosNavegacaoViewController).pageIndex!
        if(index <= 0){
            return nil
        }
        index--
        return self.viewControllerAtIndex(index)
        
    }
    
    func viewControllerAtIndex(index : Int) -> UIViewController? {
        if((self.pageTitles?.count == 0) || (index >= self.pageTitles?.count)) {
            return nil
        }
        let pageContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("conteudoFotosNavegacao") as! ConteudoFotosNavegacaoViewController
        
        let item:NSDictionary = self.images[index] as! NSDictionary
        let itemTiltes:NSDictionary = self.pageTitles[index] as! NSDictionary
        let teeexto = itemTiltes["subtitle"] as! NSString!
        pageContentViewController.imageName = item["image_url"] as! String
        pageContentViewController.titleText = teeexto as String!
        //itemTiltes["subtitle"] as! String
        pageContentViewController.pageIndex = index
        pageContentViewController.dados = self.dataDictionary
        
        
        return pageContentViewController
    }

    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
