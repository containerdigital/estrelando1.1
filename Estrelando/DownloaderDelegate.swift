//
//  DownloaderDelegate.swift
//  Estrelando
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

@objc protocol DownloaderDelegate: class{
    optional func downloaderProgressSucces(contexto:AnyObject?, download:Downloader)
    func downloaderDidFinnish(contexto:AnyObject?,download:Downloader,dados:NSData)
}

class Downloader: NSObject,NSURLConnectionDataDelegate,NSURLConnectionDelegate {
    var dataReceived:NSMutableData! = NSMutableData()
    var allSize:Float = 0
    var downloadSize:Float = 0
    var context:AnyObject?
    var download: NSURLConnection?
    var delegate:DownloaderDelegate?
    
    func iniciarDownload(url:NSURL){
        let req = NSURLRequest(URL: url)
        initRequest(req)
    }
    func initRequest(req:NSURLRequest){
        download = NSURLConnection(request: req, delegate: self, startImmediately: true)
        
    }
    //Receiving Data
    func connection(connection:NSURLConnection , didReceiveData data:NSData){
        dataReceived.appendData(data)
        delegate?.downloaderProgressSucces?(context, download: self)
    }
    func connection(connection: NSURLConnection, didFailWithError error: NSError) {
        let errorAlert = UIAlertView(title: "!", message: "Cannot init this download.", delegate: self, cancelButtonTitle: "ok")
        errorAlert.show()
    }
    
    
    //Geting the all size of archive
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        dataReceived = NSMutableData()
        allSize = Float(response.expectedContentLength)
    }
    func connectionDidFinishLoading(connection: NSURLConnection) {
        self.delegate?.downloaderDidFinnish(context, download: self, dados: dataReceived)
        dataReceived = NSMutableData()
        
    }
    func connection(connection: NSURLConnection, willCacheResponse cachedResponse: NSCachedURLResponse) -> NSCachedURLResponse? {
        var userData = NSMutableDictionary()
        if let userInfo = cachedResponse.userInfo{
            userData = NSMutableDictionary(dictionary: userInfo)
        }
        let mutableData = NSMutableData(data: cachedResponse.data)
        let storagePolicy:NSURLCacheStoragePolicy = NSURLCacheStoragePolicy.AllowedInMemoryOnly
        print("Cached Resource For : \(cachedResponse.response)")
        return NSCachedURLResponse(response: cachedResponse.response, data: mutableData, userInfo: userData as [NSObject: AnyObject], storagePolicy: storagePolicy)
    }
}
