//
//  ViewController.swift
//  Estrelando
//
//  Created by Web1 on 14/09/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

enum ContainerStatus:String {
    case LeftCollapsed  = "Left";
    case RightCollapsed = "Right";
}

class ContainerViewController: UIViewController, UIMenuDrawerContainerDelegate {
    var currentStatus:ContainerStatus = ContainerStatus.LeftCollapsed;
    var menu:MenuViewController?;
    var content:UINavigationController?;
    
    func loadPage(view:UIViewController) {
            self.content?.pushViewController(view, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Instantiate Views
        self.navigationController?.hidesBarsOnSwipe = true
        self.menu = self.storyboard!.instantiateViewControllerWithIdentifier("MainMenu") as? MenuViewController;
        self.menu!.delegate = self;
        

        self.content = self.storyboard!.instantiateViewControllerWithIdentifier("Content") as? UINavigationController;
        
       
        
        // Adjust Positions
        self.menu?.view.frame = CGRectMake(-200, 65, 200, self.view.frame.size.height);
        self.content?.view.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.size.height);
        
        
        // Add to stage
        self.view.addSubview(self.content!.view);
        self.view.addSubview(self.menu!.view);
        
        // Hide Menu
        self.menu?.view.hidden = true;
        
        
        // Add Observer
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "hideMenu", name: "hideMenu", object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "showMenu", name: "showMenu", object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "toggleMenu", name: "toggleMenu", object: nil);
        
        
        
        // Add Gestures
        let gestureSwipeL:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "hideMenu");
        gestureSwipeL.numberOfTouchesRequired = 1;
        gestureSwipeL.direction = UISwipeGestureRecognizerDirection.Left;
        
        let gestureSwipeR:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "showMenu");
        gestureSwipeR.numberOfTouchesRequired = 1;
        gestureSwipeR.direction = UISwipeGestureRecognizerDirection.Right;
        
        
        self.view.addGestureRecognizer(gestureSwipeL);
        self.view.addGestureRecognizer(gestureSwipeR);
        
        
    }
    
    // MARK: Menu Tools
    func hideMenu(){
        // Animate
        UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.menu?.view.frame = CGRectMake(-200, 65, 200, self.view.frame.size.height);
            }) { (Bool) -> Void in
                // Hide View and Set new State
                self.menu?.view.hidden = true;
                self.currentStatus = ContainerStatus.LeftCollapsed;
        }

    }
    func showMenu(){
        // Show View
        self.menu?.view.hidden = false;
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.menu?.view.frame = CGRectMake(0, 65, 200, self.view.frame.size.height);
            }) {(Bool) -> Void in
                // Set new Status
                self.currentStatus = ContainerStatus.RightCollapsed
        }
        
    }
    func toggleMenu(){
        if(self.currentStatus == ContainerStatus.LeftCollapsed){
            self.showMenu()
        } else {    
            self.hideMenu()
        }
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}

