//
//  Extensions.swift
//  Estrelando
//
//  Created by Web1 on 14/09/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    class func initWithRgba(r:Int, g:Int, b:Int, a:Int) -> UIColor {
        let red:CGFloat = CGFloat(Float(r)/255);
        let green:CGFloat = CGFloat(Float(g)/255);
        let blue:CGFloat = CGFloat(Float(b)/255);
        let alpha:CGFloat = CGFloat(Float(a)/100);
        
        return UIColor(red: red, green: green, blue: blue, alpha: alpha);
    }
}

extension NSMutableURLRequest{
    func sendPOSTValures(values:[AnyObject], withBondaryString string:String){
        self.HTTPMethod = "POST"
        self.addValue("multipart/form-data ; boundary=\(string)", forHTTPHeaderField: "Content-Type")
     }

}

extension String{
    var floatValue:Float {
        get {
            let str = NSString(string: self);
            return str.floatValue;
        }
    }

    
    
    
    
    static func converString(var string:String) -> NSMutableAttributedString?{
        var attributedString:NSMutableAttributedString = NSMutableAttributedString()
        var dataString:NSData = NSData()
        let attributedOptions:[String : AnyObject] = [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute:NSUTF8StringEncoding]
        string = string.stringByAppendingString("<style>*{font-family:'openSans-Regular'; font-size:15px; color:rgba(0,0,0,0.7);} strong{font-family: 'openSans-Bold';font-size: 15px; color:rgba(0,0,0,0.7);} i{font-family: 'openSans-Italic';font-size: 15px;} strong i {font-family: 'openSans-Italic'} a{font-family: 'openSans-Regular';font-size: 15px; color:#10a1c0} a:before {content: '&#8226'; font-family: 'openSans-Regular';font-size: 15px; color:#10a1c0} a {text-decoration: none;} img{width:300px} </style>")
        
        dataString = string.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!
        do{
            attributedString = try NSMutableAttributedString(data: dataString, options: attributedOptions, documentAttributes: nil)
            return attributedString
        
        } catch {
            print("Error")
        
        }
        return nil
    }
    
    static func colorForCategory(string:String) -> UIColor?{
        switch(string){
            case "estilo":
                return UIColor(red: 0.56862745, green: 0.0, blue: 1.0, alpha: 1.0)
               
            
            
        case "teen":
                return UIColor(red: 0.02745098 , green: 0.61176471, blue: 0.18431373, alpha: 1.0)
            
            
        case "realities":
               return UIColor(red: 1.0, green: 0.34901961, blue: 0, alpha: 1.0)
            
            
        case "séries":
                return UIColor(red: 0.02352941 , green: 0.49019608, blue: 1.0, alpha: 1.0)
            
            
        default:
                return UIColor(red: 0.61860784, green: 0.0, blue: 0.16470588, alpha: 1.0)
            
            
        }
        
            return nil
        
    }
    
    func resizeImage(format:String) -> String {
            let str = ".([0-9x]+).([a-z]+)$"
            do{
            let regex = try NSRegularExpression(pattern: str, options: .CaseInsensitive)
            let ret = regex.stringByReplacingMatchesInString(self, options: NSMatchingOptions.Anchored, range: NSMakeRange(0, self.characters.count), withTemplate: ".\(format).$2")
                return ret
            }  catch{
                print("Error")
                
        
        }
        return format
        
    }
    
    
}

extension UITextView {
    func adjustHeight() {
        let device = UIDevice.currentDevice();
        if(device.systemVersion.floatValue >= 7.0){
            let frame = self.frame
            let height:CGFloat = self.layoutManager.usedRectForTextContainer(self.textContainer).size.height+2*fabs(self.contentInset.top);
            self.frame = CGRectMake(0, 0, frame.width, height)
            
        } else {
            
            let frame = self.frame
            let height:CGFloat = self.contentSize.height;
            self.frame = CGRectMake(0, 0, frame.width, height)
        }
    }
    func getContentSize () -> CGSize {
        let cgSize = CGSize(width: CGFloat(self.frame.size.width), height: CGFloat(FLT_MAX))
        return self.sizeThatFits(cgSize)
    }
}


