//
//  Profiles.swift
//  Estrelando
//
//  Created by iOS Developer on 30/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class Profiles: Content {
    
    var thumb:UIImage?
    
    
    func initAllKeyOfProfiles(dictionary:NSDictionary){
        if let id = dictionary.objectForKey("ID") as? String{
            self.ID = id
        }
        if let link = dictionary.objectForKey("link") as? String{
            self.link = link
        }
        if let title = dictionary.objectForKey("title") as? String{
            self.title = String.converString(title)
        }
        if let thumb = dictionary.objectForKey("thumb") as? String{
            let serverHelper = ServerHelper()
            let promise = serverHelper.downloadPicture(thumb)
            promise.then({ () -> Void in
                if let result = promise.Result as? NSData{
                    let picture = UIImage(data: result)
                    self.thumb = picture
                }
            })
        }
    }
    
}
