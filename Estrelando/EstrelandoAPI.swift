//
//  EstrelandoAPI.swift
//  Estrelando
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class EstrelandoAPI: NSObject {
    static func newsCelebridades(canal:String, type:String, var pagina:Int?) -> Promise{
        var promise = Promise()
        var arrayNews = Array<Content>()
        let req = ServerHelper()
        
        if pagina == nil{
            pagina = 0
        }
        


        let ret:Promise = req.request("http://www.estrelando.com.br/\(canal)/\(type).json?")
        var key:String = "conteudo"
        if type == "perfis"{
            key = "profiles"
        }
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do {
                    let str = String(data: res, encoding: NSUTF8StringEncoding)
                    print("\(str)")
                    
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                var i = Int()
                for(i = 0; i<json.objectForKey(key)?.count; i++){
                    var news:Content!
                    if type == "perfis"{
                        news = Profiles(dictionary: (json.objectForKey(key)?.objectAtIndex(i))! as! NSDictionary)
                    } else {
                      news = Content(dictionary: (json.objectForKey(key)?.objectAtIndex(i))! as! NSDictionary)
                        
                    }
                    arrayNews.append(news)
                }
                    promise.resolv(arrayNews)
                }   catch {
                    promise.rejects("Error")
                }
            
            }
        }
       
        return promise
    }
    
    
    static func tagInside(tag:String)->Promise{
        
        let promise = Promise()
        let req = ServerHelper()

        let ret:Promise = req.request("http://www.estrelando.com.br/celebridades/perfis-interna/\(tag.lowercaseString).json")
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    let tag = TagProfile(dictionary: json)
                    
                    
                    promise.resolv(tag)
                }   catch {
                    promise.rejects("Error")
                }
                
            }
        }
        return promise

    }
    
    static func home()->Promise{
        var promise = Promise()
        var arrayFeatures = Array<MainHomeContent>()
        var arraySpecialIDs = Array<MainHomeContent>()
        var arrayConteudo = Array<MainHomeContent>()
        let req = ServerHelper()
        let ret:Promise = req.request("http://www.estrelando.com.br/home.json?count=20&page=0&cats=1,2,6,7&specials=true")
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    var i = Int()
                    for(i = 0; i<json.objectForKey("featured")?.count; i++){
                        /*
                        let specialIDs = MainHomeContent(dictionary: (json.objectForKey("specialIDs")?.objectAtIndex(i))! as! NSDictionary)
                        
                        arraySpecialIDs.append(specialIDs)
                        */
                        
                        let features = MainHomeContent(dictionary: (json.objectForKey("featured"))! as! NSDictionary)
                        
                        arrayFeatures.append(features)
                        
                        let content = MainHomeContent(dictionary: (json.objectForKey("conteudo")?.objectAtIndex(i))! as! NSDictionary)
                        
                        arrayConteudo.append(content)
                        
                        
                        
                        print(arrayFeatures,arrayConteudo)
                        
                        
                    }
                    
                    promise.resolv(arrayConteudo)
                }   catch {
                    promise.rejects("Error")
                }
                
            }
        }
        return promise
    }
    
    
    static func newsRealities(canal:String) -> Promise{
        var promise = Promise()
        var arrayNews = Array<Content>()
        let req = ServerHelper()
        let ret: Promise = req.request("http://www.estrelando.com.br/realities/notas.json")
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do{
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    var i = Int()
                     for(i = 0; i<json.objectForKey("conteudo")?.count; i++){
                        let news = Content(dictionary:(json.objectForKey("conteudo")?.objectAtIndex(i))! as! NSDictionary)
                       arrayNews.append(news)
                    }
                    promise.resolv(arrayNews)
                }   catch{
                    promise.rejects("Error")
                    }
            
            }
        }
        return promise
    }
    
    
    static func newsStyle(canal:String) -> Promise{
        var promise = Promise()
        var arrayNews = Array<Content>()
        let req = ServerHelper()
        let ret: Promise = req.request("http://www.estrelando.com.br/estilo/notas.json")
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do{
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    var i = Int()
                    for(i = 0; i<json.objectForKey("conteudo")?.count; i++){
                        let news = Content(dictionary:(json.objectForKey("conteudo")?.objectAtIndex(i))! as! NSDictionary)
                        arrayNews.append(news)
                    }
                    promise.resolv(arrayNews)
                }   catch{
                    promise.rejects("Error")
                }
                
            }
        }
        return promise
    }
    
    static func newsSeries(canal:String) -> Promise{
        var promise = Promise()
        var arrayNews = Array<Content>()
        let req = ServerHelper()
        let ret: Promise = req.request("http://www.estrelando.com.br/series/notas.json")
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do{
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    var i = Int()
                    for(i = 0; i<json.objectForKey("conteudo")?.count; i++){
                        let news = Content(dictionary:(json.objectForKey("conteudo")?.objectAtIndex(i))! as! NSDictionary)
                        arrayNews.append(news)
                    }
                    promise.resolv(arrayNews)
                }   catch{
                    promise.rejects("Error")
                }
                
            }
        }
        return promise
    }

    static func newsTeen(canal:String) -> Promise{
        var promise = Promise()
        var arrayNews = Array<Content>()
        let req = ServerHelper()
        let ret: Promise = req.request("http://www.estrelando.com.br/teen/notas.json")
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do{
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    var i = Int()
                    for(i = 0; i<json.objectForKey("conteudo")?.count; i++){
                        let news = Content(dictionary:(json.objectForKey("conteudo")?.objectAtIndex(i))! as! NSDictionary)
                        arrayNews.append(news)
                    }
                    promise.resolv(arrayNews)
                }   catch{
                    promise.rejects("Error")
                }
                
            }
        }
        return promise
    }

    
    
    static func galleryCelebridades(canal:String, var pagina:Int?) -> Promise{
        var promise = Promise()
        var arrayNews = Array<Content>()
        let req = ServerHelper()
        
        if pagina == nil{
            pagina = 0
        }
        let ret:Promise = req.request("http://http://www.estrelando.com.br/celebridades/fotos.json")
        
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do {
                    let str = String(data: res, encoding: NSUTF8StringEncoding)
                    print("\(str)")
                    
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    var i = Int()
                    for(i = 0; i<json.objectForKey("conteudo")?.count; i++){
                        let news = Content(dictionary: (json.objectForKey("conteudo")?.objectAtIndex(i))! as! NSDictionary)
                        arrayNews.append(news)
                    }
                    promise.resolv(arrayNews)
                }   catch {
                    promise.rejects("Error")
                }
                
            }
        }
        return promise
    }

    
    
    static func galleryRealities(canal:String) -> Promise{
        var promise = Promise()
        var arrayNews = Array<Content>()
        let req = ServerHelper()
        let ret: Promise = req.request("http://http://www.estrelando.com.br/realities/fotos.json")
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do{
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    var i = Int()
                    for(i = 0; i<json.objectForKey("conteudo")?.count; i++){
                        let news = Content(dictionary:(json.objectForKey("conteudo")?.objectAtIndex(i))! as! NSDictionary)
                        arrayNews.append(news)
                    }
                    promise.resolv(arrayNews)
                }   catch{
                    promise.rejects("Error")
                }
                
            }
        }
        return promise
    }
    
    
    static func galleryStyle(canal:String) -> Promise{
        var promise = Promise()
        var arrayNews = Array<Content>()
        let req = ServerHelper()
        let ret: Promise = req.request("http://http://www.estrelando.com.br/estilo/fotos.json")
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do{
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    var i = Int()
                    for(i = 0; i<json.objectForKey("conteudo")?.count; i++){
                        let news = Content(dictionary:(json.objectForKey("conteudo")?.objectAtIndex(i))! as! NSDictionary)
                        arrayNews.append(news)
                    }
                    promise.resolv(arrayNews)
                }   catch{
                    promise.rejects("Error")
                }
                
            }
        }
        return promise
    }
    
    static func gallerySeries(canal:String) -> Promise{
        var promise = Promise()
        var arrayNews = Array<Content>()
        let req = ServerHelper()
        let ret: Promise = req.request("http://http://www.estrelando.com.br/series/fotos.json")
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do{
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    var i = Int()
                    for(i = 0; i<json.objectForKey("conteudo")?.count; i++){
                        let news = Content(dictionary:(json.objectForKey("conteudo")?.objectAtIndex(i))! as! NSDictionary)
                        arrayNews.append(news)
                    }
                    promise.resolv(arrayNews)
                }   catch{
                    promise.rejects("Error")
                }
                
            }
        }
        return promise
    }
    
    static func galleryTeen(canal:String) -> Promise{
        var promise = Promise()
        var arrayNews = Array<Content>()
        let req = ServerHelper()
        let ret: Promise = req.request("http://http://www.estrelando.com.br/teen/fotos.json")
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do{
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    var i = Int()
                    for(i = 0; i<json.objectForKey("conteudo")?.count; i++){
                        let news = Content(dictionary:(json.objectForKey("conteudo")?.objectAtIndex(i))! as! NSDictionary)
                        arrayNews.append(news)
                    }
                    promise.resolv(arrayNews)
                }   catch{
                    promise.rejects("Error")
                }
                
            }
        }
        return promise
    }
}
    

