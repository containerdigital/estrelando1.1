//
//  GalleryFoto.swift
//  Estrelando
//
//  Created by iOS Developer on 28/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class GalleryFoto: NSObject {
    var ID:Int?
    var title:NSMutableAttributedString?
    var text:NSMutableAttributedString?
    var photographer:String?
    var profile_id:String?
    var thumb:String?
    var photo:String?
    var picture: Promise?
    var thumbPict : UIImage?
    var keywords:String?
    init(dictionary:NSDictionary){
        super.init()
        self.initWithAllKeysOfGallery(dictionary)
    }
    
    
    override init(){
        super.init()
    }
    
    func initWithAllKeysOfGallery(dictionary:NSDictionary){
        if let ID = dictionary.objectForKey("ID") as? Int{
            self.ID = ID
        }
        if let keywords = dictionary.objectForKey("keywords") as? String{
            self.keywords = keywords
        }
        if let title = dictionary.objectForKey("title") as? String{
            self.title = String.converString(title)
        }
        if let text = dictionary.objectForKey("text") as? String{
            self.text = String.converString(text)
        }
        if let photographer = dictionary.objectForKey("photographer") as? String{
            self.photographer = photographer
        }
        if let profile_id = dictionary.objectForKey("profile_id") as? String{
            self.profile_id = profile_id
        }
        if let thumb = dictionary.objectForKey("thumb") as? String{
            self.thumb = thumb
            let serverHelper = ServerHelper()
            let promise = serverHelper.downloadPicture(self.thumb!)
            promise.then({ () -> Void in
                if let result = promise.Result as? NSData{
                    let picture = UIImage(data: result)
                    self.thumbPict = picture
                }
            })
        }
        if let photo = dictionary.objectForKey("photo") as? String{
            self.photo = photo
            let serverHelper = ServerHelper()
            self.picture = serverHelper.downloadPicture(photo)
            self.picture!.then({ () -> Void in
                if let result = self.picture!.Result as? NSData{
                    self.picture?.Result = UIImage(data: result)
                   // self.picture = picture
                    
                }
            })
        }
    }
    
    
    
}
