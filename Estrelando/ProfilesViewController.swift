//
//  ProfilesViewController.swift
//  Estrelando
//
//  Created by iOS Developer on 30/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class ProfilesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var content:Content?
    var tag:TagProfile?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self

        let promise = EstrelandoAPI.tagInside(content!.title!.string)
        promise.then { () -> Void in
           self.tag = promise.Result as! TagProfile
            self.tableView.reloadData()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let tag = self.tag{
        
        return tag.arrayContent.count
        
        }
        
        return 0
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier:String
        identifier = "cell"
        let cell:ProfilesTableViewCell = self.tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! ProfilesTableViewCell
        let item = self.tag?.arrayContent[indexPath.row]
        
        if item?.picture != nil{
            self.tableView.rowHeight = 400.0
            cell.labelProfile.attributedText = item!.title
            cell.labelProfile.textColor = UIColor.whiteColor()
            cell.imgView.image = item!.picture
            
            
            
        } else {
            self.tableView.rowHeight = 80.0
            cell.viewConsH.constant = 90.0
            cell.imgConsH.constant = 0.0
        }
        
     
        
        return cell


    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let offSetY = self.tableView.contentOffset.y
        for cell in self.tableView.visibleCells as! [ProfilesTableViewCell]{
            let x = cell.imgView.frame.origin.x
            let w = cell.imgView.bounds.width
            let h = cell.imgView.bounds.height
            let y = ((offSetY - cell.frame.origin.y)/h) * 25
            cell.imgView.frame = CGRectMake(x, y, w, h)
            self.tableView.reloadInputViews()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
