//
//  ContentViewController.swift
//  Estrelando
//
//  Created by iOS Developer on 28/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class ContentViewController: UIViewController {
    var index:Int = Int()
    
    @IBOutlet weak var imgGallery: UIImageView!
    @IBOutlet weak var textGallery: UITextView!
    var viewPictureToZoom : UIView = UIView()
    var imgPict : UIImageView = UIImageView()
    var dados:NSMutableDictionary = NSMutableDictionary()
     var blurView : UIVisualEffectView = UIVisualEffectView()
    var galleryPhoto:GalleryFoto!
    var stringCat : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        
        
        self.view.backgroundColor = UIColor.blackColor()
        let tapZoomPicture = UITapGestureRecognizer(target: self, action: Selector("ZoomPict:"))

        self.imgGallery.userInteractionEnabled = true
        self.imgGallery.addGestureRecognizer(tapZoomPicture)
        self.imgGallery.image = self.galleryPhoto.picture!.Result as? UIImage
        self.textGallery.attributedText = self.galleryPhoto.text
        self.textGallery.textColor = UIColor.whiteColor()
        self.styleViewAndLabelColor(self.stringCat!.lowercaseString)
        
        let hamburguerItem = UIImage(named: "hmg")
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: hamburguerItem, style: UIBarButtonItemStyle.Plain, target: self, action: "toggleMenu")
        
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.4)
        //Set Logo 'Estrelando' on nav controller.
        let logo = UIImage(named: "logo-estrelando")
        let imageView = UIImageView(image: logo)
        self.navigationController?.navigationItem.titleView = imageView
        self.navigationItem.titleView = imageView

        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    
    }
    
    func ZoomPict(sender:UIImageView){
        let tapCloseZoomPicture = UITapGestureRecognizer(target: self, action: Selector("tapCloseZoomPicture:"))
    
        self.imgGallery = UIImageView(frame: CGRectMake(self.viewPictureToZoom.frame.size.width - 60 , self.viewPictureToZoom.frame.size.height - 75, 30, 30))
        
     
        self.viewPictureToZoom = UIView(frame: CGRectMake(0, self.view.frame.size.height / 3.5, self.view.frame.size.width, self.view.frame.size.height / 2))
        self.imgPict = UIImageView(frame: CGRectMake(0, 0, viewPictureToZoom.frame.size.width, viewPictureToZoom.frame.size.height))
        self.imgPict.image = self.galleryPhoto!.picture?.Result as? UIImage
        self.viewPictureToZoom.addSubview(imgPict)
        
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        self.blurView = UIVisualEffectView(effect: darkBlur)
        self.blurView.frame = self.view.bounds
        self.view.addSubview(blurView)
     
        self.blurView.userInteractionEnabled = true
        self.blurView.addGestureRecognizer(tapCloseZoomPicture)
        self.imgPict.userInteractionEnabled = true
        self.imgPict.addGestureRecognizer(tapCloseZoomPicture)
        self.viewPictureToZoom.userInteractionEnabled = true
        self.viewPictureToZoom.addGestureRecognizer(tapCloseZoomPicture)
        self.blurView.addSubview(viewPictureToZoom)
     
        
        
    }

    func tapCloseZoomPicture(sender:AnyObject){
        self.imgPict.removeFromSuperview()
        self.viewPictureToZoom.removeFromSuperview()
        self.blurView.removeFromSuperview()
    }
    
    func styleViewAndLabelColor(category:String){
        switch(category){
        case "estilo":
            self.textGallery.tintColor = UIColor(red: 0.56862745, green: 0.0, blue: 1.0, alpha: 1.0)
            break
        case "realities":
            self.textGallery.tintColor = UIColor(red: 1.0, green: 0.34901961, blue: 0, alpha: 1.0)
            break
        case "séries":
            self.textGallery.tintColor = UIColor(red: 0.02352941 , green: 0.49019608, blue: 1.0, alpha: 1.0)
            break
        case "teen":
            self.textGallery.tintColor = UIColor(red: 0.02745098 , green: 0.61176471, blue: 0.18431373, alpha: 1.0)
            break
        default:
            self.textGallery.tintColor = UIColor(red: 0.61860784, green: 0.0, blue: 0.16470588, alpha: 1.0)
            break
            
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
