//
//  MenuViewController.swift
//  Estrelando
//
//  Created by Web1 on 14/09/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

protocol UIMenuDrawerContainerDelegate:NSObjectProtocol {
    func loadPage(view:UIViewController);
}

class MenuItem:NSObject {
    var name:String;
    var screenIdentifier:String?;
    var icon:String?;
    var container:String?;
    var arguments:Dictionary<String, String> = Dictionary<String, String>();
    var opened:Bool = false;
    var color:UIColor?;
    
    var isChildren:Bool {
        get {
            if let _ = self.screenIdentifier {
                return true
            }
            return false;
        }
    }
    
    init(name:String, container:String) {
        self.name = name;
        self.container = container;
        
    }
    init(name:String, container:String, isOpen:Bool) {
        self.name = name;
        self.container = container;
        
        self.opened  = isOpen;
    }
    init(name:String, container:String, isOpen:Bool, color:UIColor) {
        self.name = name;
        self.container = container;
        self.opened  = isOpen;
        self.color = color;
    }
    init(name:String, container:String, icon:String?, screenIdentifier:String?, arguments:Dictionary<String, String>){
        
        self.name = name;
        self.container = container;
        self.icon = icon;
        self.screenIdentifier = screenIdentifier;
        
        for (keyDic, valueDic) in arguments {
            self.arguments[keyDic] = valueDic;
        }
        
    }
    
    
}

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var menuItens:Array<MenuItem> = Array<MenuItem>();
    var currentContainer:String = "Celebridades";
    var currentShowing:Array<MenuItem> = Array<MenuItem>();
    
    weak var delegate:UIMenuDrawerContainerDelegate?;
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        
        // Celebridades
        
        self.menuItens.append(MenuItem(name: "Celebridades", container: "Celebridades", isOpen:true, color:UIColor.initWithRgba(191, g: 27, b: 71, a: 100)));
        self.menuItens.append(MenuItem(name: "Notícias", container: "Celebridades", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Celebridade", "contentType":"news"]));
        self.menuItens.append(MenuItem(name: "Fotos", container: "Celebridades", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Celebridade", "contentType":"photos"]));
        self.menuItens.append(MenuItem(name: "Rankings", container: "Celebridades", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Celebridade", "contentType":"ranking"]));
        self.menuItens.append(MenuItem(name: "Perfis", container: "Celebridades", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Celebridade", "contentType":"profiles"]));
        self.menuItens.append(MenuItem(name: "Quiz", container: "Celebridades", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Celebridade", "contentType":"quizzes"]));
        
        // Estilo
        self.menuItens.append(MenuItem(name: "Estilo", container: "Estilo", isOpen:false, color:UIColor.initWithRgba(238, g: 26, b: 252, a: 100)));
        
        self.menuItens.append(MenuItem(name: "Notícias", container: "Estilo", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Estilo", "contentType":"news"]));
        self.menuItens.append(MenuItem(name: "Fotos", container: "Estilo", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Estilo", "contentType":"photos"]));
        self.menuItens.append(MenuItem(name: "Rankings", container: "Estilo", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Estilo", "contentType":"ranking"]));
        self.menuItens.append(MenuItem(name: "Quiz", container: "Estilo", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Estilo", "contentType":"quizzes"]));
        
        // Realities
        self.menuItens.append(MenuItem(name: "Realities", container: "Realities", isOpen:false, color:UIColor.initWithRgba(255, g: 102, b: 0, a: 100)));
        self.menuItens.append(MenuItem(name: "Notícias", container: "Realities", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Realities", "contentType":"news"]));
        self.menuItens.append(MenuItem(name: "Fotos", container: "Realities", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Realities", "contentType":"photos"]));
        self.menuItens.append(MenuItem(name: "Rankings", container: "Realities", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Realities", "contentType":"ranking"]));
        self.menuItens.append(MenuItem(name: "Perfis", container: "Realities", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Realities", "contentType":"profiles"]));
        self.menuItens.append(MenuItem(name: "Quiz", container: "Realities", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Realities", "contentType":"quizzes"]));
        
        // Series
        self.menuItens.append(MenuItem(name: "Séries", container: "Séries", isOpen:false, color:UIColor.initWithRgba(0, g: 144, b: 255, a: 100)));
        self.menuItens.append(MenuItem(name: "Notícias", container: "Séries", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Séries", "contentType":"news"]));
        self.menuItens.append(MenuItem(name: "Fotos", container: "Séries", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Séries", "contentType":"photos"]));
        self.menuItens.append(MenuItem(name: "Rankings", container: "Séries", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Séries", "contentType":"ranking"]));
        self.menuItens.append(MenuItem(name: "Perfis", container: "Séries", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Séries", "contentType":"profiles"]));
        self.menuItens.append(MenuItem(name: "Quiz", container: "Séries", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Séries", "contentType":"quizzes"]));
        
        // Teen
        self.menuItens.append(MenuItem(name: "Teen", container: "Teen", isOpen:false, color:UIColor.initWithRgba(0, g: 167, b: 67, a: 100)));
        self.menuItens.append(MenuItem(name: "Notícias", container: "Teen", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Teen", "contentType":"news"]));
        self.menuItens.append(MenuItem(name: "Fotos", container: "Teen", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Teen", "contentType":"photos"]));
        self.menuItens.append(MenuItem(name: "Rankings", container: "Teen", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Teen", "contentType":"ranking"]));
        self.menuItens.append(MenuItem(name: "Perfis", container: "Teen", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Teen", "contentType":"profiles"]));
        self.menuItens.append(MenuItem(name: "Quiz", container: "Teen", icon: "Cel", screenIdentifier: "Lista", arguments:["channel":"Teen", "contentType":"quizzes"]));
        
        
        
    }

    // MARK: TableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.currentShowing = Array<MenuItem>();
        
        var count:Int = 0;
        for item in self.menuItens {
            if(item.container == self.currentContainer || !item.isChildren){
                self.currentShowing.append(item);
                count++;
            }
        }
        return self.currentShowing.count;
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let item = self.currentShowing[indexPath.row];
        
        var identifier:String = "MenuItem";
        if(!item.isChildren){
            identifier = "MenuParent";
            
            if item.container == self.currentContainer {
                item.opened = true;
            } else {
                item.opened = false;
            }
        }
        
        
        let cell:MenuItemTableViewCell = self.tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! MenuItemTableViewCell;
        
        if(item.isChildren){
            cell.setChildren(item.name);
        } else {
            cell.setContainer(item.name, color: item.color!, isOpen: item.opened)
        }
        
        
        return cell;
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let item = self.currentShowing[indexPath.row];
        
        if !item.isChildren {
            self.currentContainer = item.container!;
            self.tableView.reloadData();
        } else {
            
            
            print(item.container!)
            print("\(item.name)");
            
            let tela = self.storyboard?.instantiateViewControllerWithIdentifier("Listagens") as? ListagensViewController
            tela?.stringCat = item.container!.lowercaseString
            tela?.stringType = item.name.lowercaseString
            switch(item.name.lowercaseString){
                case"notícias":
                tela?.stringType = "notas"
                self.delegate?.loadPage(tela!)
                break
                case"fotos":
                tela?.stringType = "fotos"
                self.delegate?.loadPage(tela!)
                case "rankings":
                tela?.stringType = "rankings"
                self.delegate?.loadPage(tela!)
                break
                case "perfis":
                tela?.stringType = "perfis"
                self.delegate?.loadPage(tela!)
                break
            default:
                break
            }
            
           
            
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
