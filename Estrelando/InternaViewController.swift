//
//  InternaViewController.swift
//  Estrelando
//
//  Created by iOS Developer on 15/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class InternaViewController: BaseRootViewController,UIScrollViewDelegate,UITextViewDelegate {
    var stringLink = String?()
    
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var textConstH: NSLayoutConstraint!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imgNotice: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewCat: UIView!
    @IBOutlet weak var tabBarImg: UITabBarItem!
    
    @IBOutlet weak var labelViewCategory: UILabel!
    var stringCategory:String?
    var viewShare : UIView = UIView()
    var viewPictureToZoom : UIView = UIView()
    var imgPict : UIImageView = UIImageView()
    var imgX : UIImageView = UIImageView()
    var blurView : UIVisualEffectView = UIVisualEffectView()
    @IBOutlet weak var con: NSLayoutConstraint!
    var content:Content?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.reloadAndCreatePage()
        self.textView.delegate = self
        self.scrollView.delegate = self
        self.styleViewAndLabelColor(self.stringCategory!)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func reloadAndCreatePage(){
        self.navigationController?.hidesBarsOnSwipe = true
        if let content = content{
            content.fullContent().then({ () -> Void in
                
                if let pict = content.picture {
                    self.imgNotice.image = pict
                    let viewImg = UIView(frame: CGRectMake(20, 2.5, 40, 40))
                    let imgIMG = UIImageView(frame: CGRectMake(0, 1, 40, 40))
                    let viewShare = UIView(frame: CGRectMake(90, 2.5, 40, 40))
                    let imgShare = UIImageView(frame: CGRectMake(8, 9, 21, 20))
                    let tapGesture = UITapGestureRecognizer(target: self, action: Selector("openShare:"))
                    let tapZoomPicture = UITapGestureRecognizer(target: self, action: Selector("ZoomPict:"))
                    
                    
                    self.imgNotice.userInteractionEnabled = true
                    self.imgNotice.addGestureRecognizer(tapZoomPicture)
                    
                    
                    imgShare.image = UIImage(named: "shareIcon")
                    viewShare.addSubview(imgShare)
                    viewShare.layer.masksToBounds = true
                    viewShare.layer.cornerRadius = 20
                    viewShare.layer.borderWidth = 0.5
                    viewShare.layer.borderColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7).CGColor
                    viewShare.userInteractionEnabled = true
                    
                    viewShare.addGestureRecognizer(tapGesture)
                    
                    
                    
                    self.tabBar.addSubview(viewShare)
                    
                    
                    imgIMG.image = pict
                    viewImg.addSubview(imgIMG)
                    viewImg.backgroundColor = UIColor.whiteColor()
                    viewImg.layer.masksToBounds = true
                    viewImg.layer.cornerRadius = 20
                    viewImg.layer.borderColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7).CGColor
                    viewImg.layer.borderWidth = 0.5
                    self.con.constant = 700
                    let button = UIBarButtonItem(customView: viewImg)

                    self.tabBar.addSubview(viewImg)
                    self.tabBar.hidden = true
                    
//                    self.tabBarImg.image = pict
                }
                
                self.labelTitle.numberOfLines = 4
                self.labelTitle.text = content.title?.string.uppercaseString
                
                self.labelTitle.font = UIFont(name: "open-Sans", size: 29.0)
                //self.labelTitle.textColor = UIColor(red: 0.61960784, green: 0.0, blue: 0.22745098, alpha: 1.0)
                //self.viewCat.backgroundColor = UIColor(red: 0.61960784, green: 0.0, blue: 0.22745098, alpha: 0.5)
                self.textView.attributedText = content.postText
                

                self.resizeScrollView()
                
            })
        }
    }
    

    func openShare(sender:UIView){
            let attrString = self.content!.postText as! NSAttributedString
            //let shareImage = self.content!.picture as UIImage!
            //let imgArray : [UIImage] = [shareImage]
            let objShare = [attrString]//,imgArray]
            let activityViewController = UIActivityViewController(activityItems: [objShare], applicationActivities: nil)
            self.presentViewController(activityViewController, animated: true) { () -> Void in
        
        
        //"\(self.content?.title) \(self.content?.postText)"
        
            
            
    }
    
    }
    
    func ZoomPict(sender:UIImageView){
        let tapCloseZoomPicture = UITapGestureRecognizer(target: self, action: Selector("tapCloseZoomPicture:"))
        let labelX = UILabel(frame: CGRectMake(self.imgX.frame.size.width/3.5, 0, self.imgX.frame.size.width, self.imgX.frame.size.height))
        let X = "X"
        labelX.textColor = UIColor.whiteColor()
        labelX.text = X
        self.imgX = UIImageView(frame: CGRectMake(self.viewPictureToZoom.frame.size.width - 60 , self.viewPictureToZoom.frame.size.height - 75, 30, 30))
        self.imgX.layer.masksToBounds = true
        self.imgX.layer.cornerRadius = 15.0
        self.imgX.backgroundColor = UIColor(red: 0.686, green: 0.0, blue: 0.125, alpha: 0.5)
        self.imgX.addSubview(labelX)
        self.viewPictureToZoom = UIView(frame: CGRectMake(0, self.view.frame.size.height / 3.5, self.view.frame.size.width, 200))
        self.imgPict = UIImageView(frame: CGRectMake(0, 0, viewPictureToZoom.frame.size.width, viewPictureToZoom.frame.size.height))
        self.imgPict.image = self.content?.picture
        self.viewPictureToZoom.addSubview(imgPict)

        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        self.blurView = UIVisualEffectView(effect: darkBlur)
        self.blurView.frame = self.view.bounds
        self.view.addSubview(blurView)
        labelX.userInteractionEnabled = true
        labelX.addGestureRecognizer(tapCloseZoomPicture)
        self.blurView.userInteractionEnabled = true
        self.blurView.addGestureRecognizer(tapCloseZoomPicture)
        self.imgPict.userInteractionEnabled = true
        self.imgPict.addGestureRecognizer(tapCloseZoomPicture)
        self.viewPictureToZoom.userInteractionEnabled = true
        self.viewPictureToZoom.addGestureRecognizer(tapCloseZoomPicture)
        self.blurView.addSubview(viewPictureToZoom)
        self.blurView.addSubview(self.imgX)
        
        
    }

    func tapCloseZoomPicture(sender:AnyObject){
            self.imgPict.removeFromSuperview()
            self.viewPictureToZoom.removeFromSuperview()
            self.blurView.removeFromSuperview()
    
    }
    
    
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if(maximumOffset - currentOffset) <= 40{
            UIView.animateWithDuration(1.0, delay: 0.5, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
                self.tabBar.opaque = false
                self.tabBar.hidden = false
                }, completion: { (Bool) -> Void in
                    
                    
            })
            
        }
        else {
            UIView.animateWithDuration(0.5, animations: { () -> Void in
               
                self.tabBar.hidden = true
            })
        
        }
        
    }
    
    func scrollViewDidEndScrollingAnimation(scrollView: UIScrollView) {

        self.tabBar.hidden = false
    }
    
    func resizeScrollView(){
        self.textView.scrollEnabled = false
        var size:CGFloat = self.labelTitle.frame.height
        size = size + self.imgNotice.frame.height
        size = size + self.textView.getContentSize().height
        size = size + 100

        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width, height: size)
        self.textConstH.constant = self.textView.getContentSize().height
        self.view.updateConstraints()
    
    }
    
    
    func textView(textView: UITextView, shouldInteractWithURL URL: NSURL, inRange characterRange: NSRange) -> Bool {
        
        let string = "\(URL.standardizedURL!)"
        self.content = Content(link: string)
        self.reloadAndCreatePage()
        return false
    }
    
    func stringToLink(){
        
    
    }
    
    
    func styleViewAndLabelColor(category:String){
        switch(category){
            
            
            
        case "estilo":
            self.labelTitle.textColor = UIColor(red: 0.56862745, green: 0.0, blue: 1.0, alpha: 1.0)
            self.labelViewCategory.text = category.uppercaseString
            self.viewCat.backgroundColor = UIColor(red: 0.56862745, green: 0.0, blue: 1.0, alpha: 0.5)
            self.textView.tintColor = UIColor(red: 0.56862745, green: 0.0, blue: 1.0, alpha: 1.0)
            self.tabBar.barTintColor = UIColor(red: 0.56862745, green: 0.0, blue: 1.0, alpha: 0.5)

            break
            
        case "realities":
            self.labelViewCategory.text = category.uppercaseString
            self.labelTitle.textColor = UIColor(red: 1.0, green: 0.34901961, blue: 0, alpha: 1.0)
            self.viewCat.backgroundColor = UIColor(red: 1.0, green: 0.34901961, blue: 0, alpha: 0.5)
            self.textView.tintColor = UIColor(red: 1.0, green: 0.34901961, blue: 0, alpha: 1.0)
            self.tabBar.barTintColor = UIColor(red: 1.0, green: 0.34901961, blue: 0, alpha: 0.5)
            break
            
        case "séries":
            self.labelViewCategory.text = category.uppercaseString
            self.labelTitle.textColor = String.colorForCategory(category)
            self.viewCat.backgroundColor = UIColor(red: 0.02352941 , green: 0.49019608, blue: 1.0, alpha: 0.5)
            self.textView.tintColor = UIColor(red: 0.02352941 , green: 0.49019608, blue: 1.0, alpha: 1.0)
            self.tabBar.barTintColor = UIColor(red: 0.02352941 , green: 0.49019608, blue: 1.0, alpha: 1.0)
            break
            
        case "teen":
            self.labelViewCategory.text = category.uppercaseString
            self.labelTitle.textColor = String.colorForCategory(category)
            self.viewCat.backgroundColor = UIColor(red: 0.02745098 , green: 0.61176471, blue: 0.18431373, alpha: 0.5)
            self.textView.tintColor = UIColor(red: 0.02745098 , green: 0.61176471, blue: 0.18431373, alpha: 1.0)
            self.tabBar.barTintColor = UIColor(red: 0.02745098 , green: 0.61176471, blue: 0.18431373, alpha: 1.0)
            break
            
            
            
        default:
            self.labelViewCategory.text = category.uppercaseString
            self.textView.tintColor = UIColor(red: 0.61860784, green: 0.0, blue: 0.16470588, alpha: 1.0)
            self.labelTitle.textColor = String.colorForCategory(category)
                self.viewCat.backgroundColor = UIColor(red: 0.61860784, green: 0.0, blue: 0.16470588, alpha: 0.5)
            self.tabBar.barTintColor = UIColor(red: 0.61860784, green: 0.0, blue: 0.16470588, alpha: 1.0)
            break
            
        }
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
