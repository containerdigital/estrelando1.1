//
//  ConteudoFotosNavegacaoViewController.swift
//  Estrelando
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class ConteudoFotosNavegacaoViewController: UIViewController {
    @IBOutlet weak var labelMateria:UILabel?
    @IBOutlet weak var imagemMateria:UIImageView?
    var readMore:Bool = false;
    var pageIndex:Int?
    var titleText: String?
    var imageData: NSMutableData?
    var imageName:String?
    var urlFoto:NSURL?
    var dataFoto:NSMutableData?
    var dados:NSMutableDictionary?

    
    @IBOutlet weak var consHeight: NSLayoutConstraint!
    @IBOutlet weak var posTxt: NSLayoutConstraint!
    
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var labelTextoMateria: UILabel!
    @IBOutlet weak var verMaisButton: UIButton!
    @IBOutlet weak var fecharButton:UIButton!
    @IBAction func fecharButton(sender: UIButton) {
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
