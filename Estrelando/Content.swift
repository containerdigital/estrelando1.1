//
//  MateriaClass.swift
//  Estrelando
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class Content: NSObject{

    var ID : String?
    var categories : String?
    var foto1 : String?
    var postDate : Int?
    var summary : String?
    var title : NSMutableAttributedString?
    var type : String?
    var picture: UIImage?
    var link: String?
    var postText:NSMutableAttributedString?
    let strongSelf = self
    
    
    
    
    init(link:String){
        super.init()
        self.link = link
    }

    
    init(dictionary:NSDictionary){
        super.init()
        
        self.initAllKeys(dictionary)
        
        
    }
    
    
    func initAllKeys(dictionary:NSDictionary){
        if let ID = dictionary.objectForKey("ID") as? String{
            self.ID = ID
        }
        
        if let categories = dictionary.objectForKey("categories") as? String{
            self.categories = categories
        }
        
        if let foto1 = dictionary.objectForKey("foto1") as? String{
            self.foto1 = foto1 //.resizeImage("230x220")
            let serverHelper = ServerHelper()
            let promise = serverHelper.downloadPicture(self.foto1!)
            
            promise.then({ [weak self]  in
                if let result = promise.Result as? NSData{
                    let picture = UIImage(data: result)
                    self!.picture = picture
                }
                })
            
        }
        
        if let link = dictionary.objectForKey("link") as? String{
            self.link = link
        }
        
        if let postDate = dictionary.objectForKey("postDate") as? Int{
            self.postDate = postDate
        }
        
        
        if let summary = dictionary.objectForKey("summary") as? String {
            self.summary = summary
        }
        
        if let title = dictionary.objectForKey("title") as? String{
            //self.title = title
            
            self.title = String.converString(title)
            
            
        }
        
        if let postText = dictionary.objectForKey("postText") as? String{
            self.postText = String.converString(postText)
        }
        
        
        if let type = dictionary.objectForKey("type") as? String{
            self.type = type
        }

    
    }
    
    func matchesForRegexInText(regex: String!, text: String!) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matchesInString(text,
                options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substringWithRange($0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    func toGallery()->Gallery
    {
        if let link = self.link{
        let gallery = Gallery(link: link)
        
            return gallery
        }
        
        return Gallery(link: self.link!)
    }
    
    func fullContent() -> Promise{
        let promise = Promise()
        let req = ServerHelper()
        if let linkNotice = self.link  {
            
            let ret:Promise = req.request("\(linkNotice).json")
            ret.then { () -> Void in
                if let res = ret.Result as? NSData?{
                 do {
                        let str = String(data: res!, encoding: NSUTF8StringEncoding)
                        print("\(str)")
                        let json = try NSJSONSerialization.JSONObjectWithData(res!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                        self.initAllKeys(json)
                        promise.resolv(self)
                    

                    
                 } catch {
                        promise.rejects("Error")
                    }
            }
                
            }
            
        }
        else {
            promise.rejects("Error")
        }

        return promise

    }

}
