//
//  HomeCollectionViewController.swift
//  Estrelando
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit



class HomeCollectionViewController: UICollectionViewController{
    var array:Array<Content> = Array<Content>()
    var layout = UICollectionViewFlowLayout()
    var currentPage:Int = 0
    let reuseIdentifier = "cell"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.loadPage(1)
        
        let promise = EstrelandoAPI.newsCelebridades("celebridades",type:"notas", pagina: self.currentPage)
        
       // let promise = EstrelandoAPI.home()
        promise.then { () -> Void in
                self.array = promise.Result as! Array<Content>
                print("\(self.array)")

                self.collectionView?.reloadData()
                self.collectionView?.invalidateIntrinsicContentSize()
        }
        
        /*
        let strechyLayout = listUICollectionViewFlowLayout()
            strechyLayout.headerReferenceSize = CGSizeMake(320, 160)
        
            collectionView?.collectionViewLayout = strechyLayout

            self.layout.itemSize = CGSizeMake(150, 200)
            self.layout.minimumInteritemSpacing = 0.0
            self.layout.minimumLineSpacing = 5.0
  

            collectionView?.collectionViewLayout = self.layout
        */

        collectionView?.registerClass(TopCollectionViewCell.classForCoder(), forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withReuseIdentifier: "celula")

       
    

        
        

        
        
        /*
        dict = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments, error:nil) as! NSMutableDictionary
            self.array = dict.objectForKey("conteudo") as! NSMutableArray
            print("\(self.array)")
          */
            
        
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
    

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    
    
    
       
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.array.count
    }


    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        
        var header:TopCollectionViewCell = TopCollectionViewCell()
        
       if(kind == UICollectionElementKindSectionHeader) {
            header = (collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "celula", forIndexPath: indexPath) as? TopCollectionViewCell)!
            var bounds = CGRect()
            bounds = header.bounds
        
        
        
        
            let img = UIImageView(frame: CGRectMake(0, 0, self.view.bounds.size.width, 310))
            let pict = UIImage(named: "logo-estrelando")
                img.image = pict
        
            header.addSubview(img)

            header.labelTop?.text = "Estrela"
            header.imageViewTop?.image = UIImage(named: "logo-estrelando")

        
        //header.labelTop.text = "Estrelando blá blá blá"
            //
        
        }
        
        if(kind == UICollectionElementKindSectionFooter){
            header = (collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "celula", forIndexPath: indexPath) as? TopCollectionViewCell)!
            var bounds = CGRect()
            bounds = header.bounds
          
            header.labelTop.text = "Estrelando blá blá blá"
            header.imageViewTop.image = UIImage(named: "logo-estrelando")
          
            
                self.collectionView?.reloadData()
            
        
        }
        
        
        
        //
        
        return header
        
    }
    

  
    func updateSectionHeader(header:UICollectionReusableView,forIndexPath:NSIndexPath){
    
        let text = String()
        
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        if(self.view.frame.size.width > 375){
            
            return CGSize(width: self.view.frame.size.width/2, height: 231);
        }
        else{
        
            return CGSize(width: self.view.frame.size.width/2, height: 231);
        }
        
    }
    /*
    func collectionView(collectionView:UICollectionView )layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
    {
    
    return 10;
    }
    */
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        

        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! HomeCollectionViewCell
        
        let item = self.array[indexPath.row]
        
        
       /*

        let matches = matchesForRegexInText("([0-9]+)", text: string)
        print("\(matches)")
        matches.contains("6")

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) { () -> Void in
           // let strPicture = self.array.objectAtIndex(indexPath.row).objectForKey("foto1") as! String
            let urlPicture = NSURL(string: strPicture)
            let dataPicture = NSData(contentsOfURL: urlPicture!)
            let img = UIImage(data: dataPicture!)
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
              cell.imageViewCell.image = img
              cell.imageViewCell.bringSubviewToFront(cell.viewCategoria)
            })
        }
        */
        /*
        switch matches[0] {
            case "6":
                
            break
            
            default:
                cell.viewCategoria.backgroundColor = UIColor(colorLiteralRed: (255/0), green: (0/255), blue: (58/255), alpha: 1.0)
            break
            
            
        }
        */
        
        
        // Configure the cell
       
        cell.labelCategoria.font = UIFont(name: "openSans-Regular", size: 14.0)
        cell.labelCategoria.textColor = UIColor.whiteColor()
        cell.imageViewCell.image = item.picture
        cell.labelCategoria.text = "Celebridades"
        cell.labelMateria.numberOfLines = 4
        cell.labelMateria.attributedText = item.title
        //let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath)
    
      
    
        return cell
    }

    
    

    func loadPage(page:Int){
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        let promise = Promise()
        self.currentPage = page
        let start = page + 1
        let stringUrl = EstrelandoAPI.newsCelebridades("celebridades",type:"notas", pagina: start)
        stringUrl.then { () -> Void in
            
            let result = promise.Result as! Array<Content>
            self.array.appendContentsOf(result)
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
        }
        
        
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let item = self.array[indexPath.row]
        
        print(item.type)
        
        switch(item.type!){
            
        case "nota":
            let tela = self.storyboard?.instantiateViewControllerWithIdentifier("internaNotas") as? InternaViewController
            tela?.content = item
          //  tela?.stringCategory = self.stringCat
            self.navigationController?.pushViewController(tela!, animated: true)
            break
        case "gallery":
            let tela = self.storyboard?.instantiateViewControllerWithIdentifier("galleryNavigation") as? GalleryViewController
            tela?.gallery = item.toGallery()
            //tela?.stringCategory = self.stringCat
            self.navigationController?.pushViewController(tela!, animated: true)
            break
        case "perfis":
            let tela = self.storyboard?.instantiateViewControllerWithIdentifier("profiles") as? ProfilesViewController
            tela?.content = item
            self.navigationController?.pushViewController(tela!, animated: true)
            break
        default:
            break
        }
        
    }

    }
    
    
    //MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */


