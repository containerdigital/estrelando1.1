//
//  ProfilesTableViewCell.swift
//  Estrelando
//
//  Created by iOS Developer on 30/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class ProfilesTableViewCell: UITableViewCell {

    @IBOutlet weak var viewConsH: NSLayoutConstraint!
    @IBOutlet weak var imgConsH: NSLayoutConstraint!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var labelProfile:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewProfile.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
