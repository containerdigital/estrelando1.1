//
//  MenuItemTableViewCell.swift
//  Estrelando
//
//  Created by Web1 on 14/09/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class MenuItemTableViewCell: UITableViewCell {

    @IBOutlet weak var title:UILabel?;
    @IBOutlet weak var indentionLeft:NSLayoutConstraint?;
    @IBOutlet weak var imageOpenClose:UIImageView?;

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.title!.font = UIFont(name: "OpenSans-Regular.ttf", size: 20.0);
    }

    func setContainer(title:String, color:UIColor, isOpen:Bool){
        self.title?.text = title;
        self.backgroundColor = color;
        self.indentionLeft?.constant = 20;
        if(isOpen){
            self.imageOpenClose?.image = UIImage(named: "menuItemOpen.png");
        } else {
            self.imageOpenClose?.image = UIImage(named: "menuItemClosed.png");
        }
    }
    func setChildren(title:String){
        self.title?.text = title;
        self.indentionLeft?.constant = 40;
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
