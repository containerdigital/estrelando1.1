//
//  GalleryViewController.swift
//  Estrelando
//
//  Created by iOS Developer on 28/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class GalleryViewController: UIPageViewController,UIPageViewControllerDataSource,UIPageViewControllerDelegate {

    var index:Int = Int()
    var contentOfGallery:ContentViewController = ContentViewController()
    var gallery : Gallery?
    var pageViewController = UIPageViewController()
    var arrayItens = NSMutableArray()
    var stringCategory:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pageViewController.dataSource = self
        self.pageViewController.delegate = self
   

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.gallery?.fullContent().then({ () -> Void in
            self.pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("galleryNavigation") as! UIPageViewController
           /*
            let pageContentViewController = self.viewControllerForIndex(self.index)
            
            self.pageViewController.setViewControllers([pageContentViewController!], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)

            self.addChildViewController(self.pageViewController)
            self.view.addSubview(self.pageViewController.view)
            self.pageViewController.didMoveToParentViewController(self)
*/
            self.index = 0
            self.pageViewController.dataSource = nil;
            self.pageViewController.dataSource = self;
            let pageContentViewController = self.viewControllerForIndex(self.index)
           // self.pageViewController.setViewControllers([pageContentViewController!], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)
            
            self.pageViewController.setViewControllers([pageContentViewController!], direction: .Forward, animated: false, completion: nil)
            
            
            /* We are substracting 30 because we have a start again button whose height is 30*/
            self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
            self.addChildViewController(self.pageViewController)
            self.view.addSubview(self.pageViewController.view)
            self.pageViewController.didMoveToParentViewController(self)

            
        })
        
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
       // var index = contentOfGallery.index
        self.index--
        contentOfGallery.index = self.index
        if self.index < 0 {
            return nil
        }
        
        return self.viewControllerForIndex(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
      //  var index = contentOfGallery.index
       
        if let gallery = gallery?.photos.count{
            
            if(self.index >= gallery){
                return nil
                
            }
        }
        self.index++
        contentOfGallery.index = index
        
        /*
        if(index >= gallery?.photos.count){
            return nil

        }
        */
        return self.viewControllerForIndex(index)
    }
    
    func viewControllerForIndex(index:Int) -> UIViewController?{
        
       
        if((self.gallery?.photos.count == 0) || (index >= self.gallery?.photos.count)){
            return nil
        }
        
        let contentPhotos = self.storyboard?.instantiateViewControllerWithIdentifier("galleryContent") as! ContentViewController
        let galeria = gallery!.photos[index]
        print(gallery!.photos.count)
        
        contentPhotos.index = index
        contentPhotos.stringCat = self.stringCategory
        contentPhotos.galleryPhoto = galeria
      //  contentPhotos.textGallery.text = galeria.title
        //contentPhotos.imgGallery.image = galeria!.picture
      
        return contentPhotos
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return self.arrayItens.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
            return self.arrayItens.count
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
