//
//  ListagensViewController.swift
//  Estrelando
//
//  Created by iOS Developer on 13/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

    protocol contentForList:NSObjectProtocol{
        var title:String { get set }
        var link:String{get set}
        var picture:Promise{get set}
        

    }

class ListagensViewController: BaseRootViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var labelCat: UILabel!
    var stringCat:String = String()
    var array:Array<Content> = Array<Content>()
    var currentPage:Int = 0
    var stringType:String = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.toggleMenu()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.loadCategorie()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func loadCategorie(){
        
        switch(self.stringCat){
        case "celebridades":
            let promise = EstrelandoAPI.newsCelebridades("\(self.stringCat.lowercaseString)",type: "\(self.stringType.lowercaseString)" ,pagina: self.currentPage)
            promise.then { () -> Void in
                self.array = promise.Result as! Array<Content>
                self.tableView?.reloadData()
                
            }
        break
            
        case "estilo":
            let promise = EstrelandoAPI.newsCelebridades("\(self.stringCat.lowercaseString)",type: "\(self.stringType.lowercaseString)" ,pagina: self.currentPage)
            promise.then { () -> Void in
                self.array = promise.Result as! Array<Content>
                self.tableView?.reloadData()
                
            }
            break
            
        case "realities":
            let promise = EstrelandoAPI.newsCelebridades("\(self.stringCat.lowercaseString)",type: "\(self.stringType.lowercaseString)" ,pagina: self.currentPage)
            promise.then { () -> Void in
                self.array = promise.Result as! Array<Content>
                self.tableView?.reloadData()
                
            }
            
            break
            
        case "séries":
            let series = "series"
            let promise = EstrelandoAPI.newsCelebridades("\(series.lowercaseString)",type: "\(self.stringType.lowercaseString)" ,pagina: self.currentPage)
            promise.then { () -> Void in
                self.array = promise.Result as! Array<Content>
                self.tableView?.reloadData()
                
            }
            break
            
        case "teen":
        let promise = EstrelandoAPI.newsCelebridades("\(self.stringCat)",type: "\(self.stringType)" ,pagina: self.currentPage)
            promise.then { () -> Void in
                self.array = promise.Result as! Array<Content>
                self.tableView?.reloadData()
                
            }
            break
        /*
            case ("Celebridades","Fotos"):
            let promise = EstrelandoAPI.galleryCelebridades("Celebridades", pagina: self.currentPage)
            promise.then { () -> Void in
                self.array = promise.Result as! Array<Content>
                self.tableView?.reloadData()
                
            }
            break
            
            case ("Estilo","Fotos"):
            let promise = EstrelandoAPI.galleryStyle("Style")
            promise.then { () -> Void in
                self.array = promise.Result as! Array<Content>
                self.tableView?.reloadData()
                
            }
            break
            
            case ("Realities","Fotos"):
            let promise = EstrelandoAPI.galleryRealities("Realities")
            promise.then { () -> Void in
                self.array = promise.Result as! Array<Content>
                self.tableView?.reloadData()
                
            }
            break
            
            case ("Séries","Fotos"):
            let promise = EstrelandoAPI.gallerySeries("Series")
            promise.then { () -> Void in
                self.array = promise.Result as! Array<Content>
                self.tableView?.reloadData()
                
            }
            break
            
            case ("Teen","Fotos"):
            let promise = EstrelandoAPI.galleryTeen("Teen")
            promise.then { () -> Void in
                self.array = promise.Result as! Array<Content>
                self.tableView?.reloadData()
                
            }
            break
            
    
            
          */
            
        default:
            break
            
        }
        self.navigationController?.hidesBarsOnSwipe = true
        self.tableView.delegate = self
        self.tableView.dataSource = self
    
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        var identifier:String
        if(indexPath.row == 0){
            identifier = "celula"
        
        }
        else {
            identifier = "cell"
        }
        let cell:Listagens2TableViewCell = self.tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! Listagens2TableViewCell
        
      
        cell.imageNot?.hidden = false
        cell.setEditing(false, animated: true)
        
        
        let item = self.array[indexPath.row]
        
        if(identifier == "celula"){
            cell.styleCategory(self.stringCat)
            cell.labelTitle.text = self.stringCat.uppercaseString
            cell.labelTitle.textColor = String.colorForCategory(self.stringCat)
        }

        if(identifier == "cell"){
//            self.tableView.rowHeight = 90.0
            cell.labelTitle.attributedText = item.title
            cell.labelTitle.numberOfLines = 4
            cell.imageNot?.image = item.picture
            cell.imgConstH?.constant = 60.0
            cell.imgConstW?.constant = 60.0
            cell.updateConstraintsIfNeeded()
        }
        
        
        
        
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let item = self.array[indexPath.row]
        
        print("\(item)")
    
        
        switch(self.stringType){
        
        case "notas":
            let tela = self.storyboard?.instantiateViewControllerWithIdentifier("internaNotas") as? InternaViewController
            tela?.content = item
            tela?.stringCategory = self.stringCat
            self.navigationController?.pushViewController(tela!, animated: true)
            break
        case "fotos":
            let tela = self.storyboard?.instantiateViewControllerWithIdentifier("galleryNavigation") as? GalleryViewController
            tela?.gallery = item.toGallery()
            tela?.stringCategory = self.stringCat
            self.navigationController?.pushViewController(tela!, animated: true)
            break
        case "perfis":
            let tela = self.storyboard?.instantiateViewControllerWithIdentifier("profiles") as? ProfilesViewController
            tela?.content = item
            self.navigationController?.pushViewController(tela!, animated: true)
            break
        default:
            break
        }
        
        
        
    }
    
    
    /*
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let offSetY = self.tableView.contentOffset.y
        for cell in self.tableView.visibleCells as! [Listagens2TableViewCell]{
            let x = cell.view!.frame.origin.x
            let w = cell.view!.bounds.width
            let h = cell.view!.bounds.height
           let y = ((offSetY - cell.frame.origin.y)/h) * 25
            cell.view?.frame = CGRectMake(x, y, w, h)
            
        }
    }
    
    
    */
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
