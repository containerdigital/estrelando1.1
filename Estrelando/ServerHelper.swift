//
//  ServerHelper.swift
//  Estrelando
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

protocol ServerHelperDelegate:NSObjectProtocol{
    func requestEnded(data:NSData)
}


class ServerHelper: NSObject,DownloaderDelegate {
    let downloader = Downloader()
    var delegate:ServerHelperDelegate?
    var updatePending = Array<Downloader>()
    var recentUpdate = false
    
    func request(page:String)->Promise {
        let promise = Promise()
        let url = NSURL(string: page)
        
        if(url == nil){
            promise.rejects("INVALID_URL")
            return promise
        }
        
        let req = NSMutableURLRequest(URL: url!)
        self.downloader.delegate = self
        self.downloader.initRequest(req)
        self.downloader.context = promise
        return promise
    }
    func requestToPost(page:String, context:AnyObject,HttpBody:[AnyObject])->Promise{
        let promise = Promise()
        let url = NSURL(string: page)
        
        if(url == nil){
            promise.rejects("INVALID_URL")
            return promise
        }
        
        let req = NSMutableURLRequest(URL: url!)
        
        /*Post values , of an request.
            req.sendPOSTValures(HttpBody, withBondaryString: AnyString)
        */
        
        self.downloader.delegate = self
        self.downloader.initRequest(req)
        self.downloader.context = promise
        return promise
    }
    func downloaderDidFinnish(contexto: AnyObject?, download: Downloader, dados: NSData) {
        let promise = contexto as! Promise
        promise.resolv(dados)

    }
    
    func downloadPicture(url:String)->Promise{
        return self.request(url)
    }
    
    
}
