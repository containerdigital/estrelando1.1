//
//  TagProfile.swift
//  Estrelando
//
//  Created by iOS Developer on 30/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class TagProfile: NSObject {
    var title:String?
    var occupation:String?
    var birthDay:String?
    var endDate:String?
    var arrayContent:Array<Content> = Array<Content>()
    
     init(dictionary:NSDictionary){
        super.init()
        if let title = dictionary.objectForKey("title") as? String{
            self.title = title
        }
        if let occupation = dictionary.objectForKey("occupation")as? String{
            self.occupation = occupation
        }
        if let birthDay = dictionary.objectForKey("birthday") as? String{
            self.birthDay = birthDay
        }
        if let endDate = dictionary.objectForKey("endDate") as? String{
            self.endDate = endDate
        }
        if let conteudo = dictionary.objectForKey("conteudo") as? NSArray{
            for var i = 0 ; i < conteudo.count; i++ {
                let content = Content(dictionary: conteudo[i] as! NSDictionary)
                self.arrayContent.append(content)
            }
        }
    
    }
    
    func mergeTags(tag:TagProfile){
        for var i = 0 ; i < tag.arrayContent.count; i++ {
            self.arrayContent.append(tag.arrayContent[i] as Content)
        }
    }
}
