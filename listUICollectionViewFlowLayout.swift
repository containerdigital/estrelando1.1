//
//  listUICollectionViewFlowLayout.swift
//  Estrelando
//
//  Created by iOS Developer on 09/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class listUICollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    
    override func layoutAttributesForElementsInRect(rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var insetes = UIEdgeInsets()
        var offSet = CGPoint()
        var minY = -insetes.top
        insetes = (collectionView?.contentInset)!
        offSet = (collectionView?.contentOffset)!
        
        // Getting the superClass Attributes
        
        let attributes = super.layoutAttributesForElementsInRect(rect)
        
        if(offSet.y < minY){
            let deltaY = fabs(offSet.y - minY)
            let attrs:UICollectionViewLayoutAttributes = UICollectionViewLayoutAttributes()
            
            for attrs in attributes! {
                
                let kind:String = String(attrs .representedElementKind)
                if kind == UICollectionElementKindSectionHeader{
                    let headerSize = self.headerReferenceSize
                    var headerRect = attrs.frame
                    headerRect.size.height = max(minY, headerSize.height + deltaY)
                    headerRect.origin.y = headerRect.origin.y - deltaY
                    attrs.frame = headerRect
                    break;
                }
                
            }
            
        }
        return attributes
        
    }
    
    
}
