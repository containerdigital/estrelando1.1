//
//  TopCollectionViewCell.swift
//  Estrelando
//
//  Created by iOS Developer on 09/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class TopCollectionViewCell: UICollectionReusableView {
    
    @IBOutlet weak var labelTop: UILabel!
    @IBOutlet weak var imageViewTop: UIImageView!
}
